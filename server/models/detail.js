'user strict';
var sql = require('../db');

//Task object constructor
var Detail = function (detail) {
    this.name = detail.name;
    this.email = detail.email;
    this.phone = detail.phone;
    this.created_at = new Date();
};

Detail.createDetail = function (details, result) {
    console.log(details)
    sql.query("INSERT INTO details set ?", details, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            console.log(res);
            sql.query("Select * from details where id = ?", res.insertId, function (err2, res2) {
                result(null, res2);
            });
        }
    });
};

Detail.getAllDetails = function (result) {
    sql.query("Select * from details", function (err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        }
        else {
            console.log('tasks : ', res);

            result(null, res);
        }
    });
};

Detail.getDetailById = function (detailId, result) {
    sql.query("Select * from details where id = ? ", detailId, function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        }
        else {
            result(null, res);

        }
    });
};

Detail.updateById = function (id, detail, result) {
    console.log(detail)
    sql.query("UPDATE details SET name = ?, email = ?, phone = ?  WHERE id = ?", [detail.name, detail.email,detail.phone, id], function (err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        }
        else {
            result(null, res);
        }
    });
};

module.exports = Detail;