'use strict';
const express = require('express');
const app = express();
const apiRouter = require('./routes');

const bodyParser = require('body-parser');
const cors = require('cors');

app.use(express.json());
app.use('/api', apiRouter);
// app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(process.env.PORT || '3001', () => {
    console.log(`The server is running at the port: ${process.env.PORT || '3001'}`);
});