'use strict';
var Detail = require('../models/detail.js');

exports.index = function (req, res) {
    res.send('NOT IMPLEMENTED: Site Home Page');
};

exports.listDetails = function (req, res) {
    Detail.getAllDetails(function (err, task) {
        console.log('controller')
        if (err)
            res.send(err);
        res.send(task);
    });
};

exports.getDetail = function (req, res) {
    Detail.getDetailById(req.params.detailId, function (err, detail) {
        if (err)
            res.send(err);
        res.json(detail);
    });
};

exports.updateDetail = function (req, res) {
    Detail.updateById(req.params.detailId, new Detail(req.body), function (err, detail) {
        if (err)
            res.send(err);
        res.json(detail);
    });
};
exports.insertDetail = function (req, res) {
    var newDetail = new Detail(req.body);
    console.log(req)
    //handles null error 
    if (!newDetail.name || !newDetail.phone) {
        res.status(400).send({ error: true, message: 'Please provide name/phone' });
    }
    else {
        Detail.createDetail(newDetail, function (err, detail) {
            if (err)
                res.send(err);
            res.json(detail);
        });
    }
}
