'use strict';
const express = require('express');
const router = express.Router();

var taskController = require('../controllers/TaskController');
var detailController = require('../controllers/DetailsController');

router.get('/', (req,res,next) => {
    res.json({
        test : 'test',
        test2 : 'test2'
    });
});

router.get('/home', function (req, res) {
    res.send('Wiki home page');
})

router.post('/about', function (req, res) {
    res.send('About this wiki');
})

router.get('/users/:userId', function (req, res) {
    res.send(req.params);
})

// router.get('/book', taskController.index);
router.get('/task', taskController.list_all_tasks);

router.get('/task/:taskId', taskController.read_a_task);

// routes for details start
router.get('/detail', detailController.listDetails);
router.get('/detail/:detailId', detailController.getDetail);
router.post('/detail/:detailId', detailController.updateDetail);
router.post('/insert', detailController.insertDetail);

module.exports = router;

